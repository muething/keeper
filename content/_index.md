Zur sicheren Speicherung und Verteilung von Zugangsdaten zu zentralen Systemen verwendet Müthing ein
cloud-basiertes Passwort-Management auf Basis von [Keeper](https://keepersecurity.eu). Im folgenden
finden Sie einige Hinweise zur Verwendung sowie zur Behandlung von Daten in diesem System.

# Grundlagen

Keeper ist ein cloud-basierter Passwort-Tresor, in dem alle Daten grundsätzlich verschlüsselt
gespeichert werden (zero knowledge). Hierzu vergibt jeder Nutzer ein Master-Passwort, das verwendet
wird, um die Daten, auf die er zugreifen kann, zu verschlüsseln. Dieses Passwort ist dem System
**nicht** bekannt, da alle Ver- und Entschlüsselung auf dem Endgerät des Benutzers passiert.

Das System bietet verschiedene Clients (Desktop-Anwendungen, Browser-Erweiterungen,
Smartphone-Apps), deren Inhalt automatisch synchronisiert wird. Die Einträge innerhalb des Tresors
liegen in Ordnern, die mit anderen Benutzern geteilt werden können, um so gemeinsamen Zugriff auf
Passwörter etc. zu bekommen. Ein Eintrag kann in mehreren Ordnern mit unterschiedlichen Rechten
freigegeben werden.

# Verwendung

Wenn Sie diese Seite lesen, haben Sie wahrscheinlich eine Email mit einer Einladung bekommen. Bevor
Sie diese annehmen, lesen Sie bitte folgende Hinweise gründlich durch.

## Datenschutz

Das System speichert Ihre Email-Adresse, welche als Identifikationsmerkmal für Ihr Benutzerkonto
dient. Darüber hinaus werden zu Audit-Zwecken sämtliche Logins in das System und alle Zugriffe auf
einzelne Einträge protokolliert, zusammen mit dem verwendeten Client und der IP-Adresse, von der aus
der Zugriff stattfand.

Um sicherzustellen, dass wichtige Zugangsdaten nicht durch ein vergessenes Master-Passwort oder
durch Ausscheiden eines Mitarbeiters / Partners verloren gehen, können besonders berechtigte
Müthing-Administratoren Benutzerkonten löschen und sämtliche dort gespeicherten Einträge auf ihr
eigenes Konto übertragen. Speichern Sie daher **ausschließlich Müthing-bezogene Daten** in diesem
System, niemals Daten zu persönlichen Logins wie privaten Email-Accounts etc.

Dieser Übertragbarkeit von Daten aus Ihrem Benutzerkonto müssen Sie nach der Anmeldung explizit
zustimmen. Dies ist nicht optional -- falls Sie nicht zustimmen, wird Ihr Account nach einigen Tagen
automatisch deaktiviert.

## Benutzerkonto

Ihr Benutzerkonto ist mit Ihrer Email-Adresse verknüpft, mit der Sie sich am System anmelden können.

-   Zum Anmelden müssen Sie grundsätzlich Ihr Master-Passwort angeben. Dieses Passwort muss gewissen
    Komplexitätsanforderungen genügen, die vom System vorgeschrieben werden.
    
-   Um Ihr Konto bei Verlust des Master-Passworts wiederherstellen zu können, legen Sie beim
    Erstellen des Kontos eine Sicherheitsfrage und -antwort fest. **Verwenden Sie hier bitte keine
    der Standard-Fragen!** Wählen Sie den Punkt "Eigene Frage festlegen" und wählen Sie eine
    Information, die nicht einfach durch Dritte bzw. durch einen Wörterbuch-Abgleich erraten werden
    kann.
  
-   Da das System äußerst sensible Daten speichert, schreiben wir zwingend eine
    [Zwei-Faktor-Authentifizierung](https://de.wikipedia.org/wiki/Zwei-Faktor-Authentifizierung)
    vor. Hierfür verwenden Sie bitte Ihr Smartphone mit einer Standard-App für zeitbasierte
    Token-Authentifizierung, z.B. Microsoft Authenticator
    ([Android](https://play.google.com/store/apps/details?id=com.azure.authenticator),
    [iOS](https://itunes.apple.com/de/app/microsoft-authenticator/id983156458?mt=8)) oder Google
    Authenticator
    ([Android](https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2&hl=de),
    [iOS](https://itunes.apple.com/de/app/google-authenticator/id388497605?mt=8)).
    
    **Wichtig:** Notieren Sie sich unbedingt die einmal verwendbaren Recovery-Codes, die nach dem
    Aktivieren der Zwei-Faktor-Authentifizierung angezeigt werden, und bewahren Sie diese an einem
    sicheren Ort auf! Sollten Sie Ihr Telefon verlieren, ist es ohne diese Codes unmöglich, auf Ihr
    Konto zuzugreifen!
    
    Falls Sie einen [FIDO U2F](https://de.wikipedia.org/wiki/U2F)-Stick (z.B. YubiKey) besitzen,
    können Sie diesen nach der Aktivierung der Zwei-Faktor-Authentifizierung als alternativen
    zweiten Faktor registrieren und danach in den Desktop-Programmen und dem Web-Client (mit Chrome)
    verwenden.

## Bedienung

Sie können über [native Clients](https://keepersecurity.com/de_DE/download.html) für alle
gängigen Betriebssysteme sowie Android und iOS,
[Browser-Erweiterungen](https://keepersecurity.com/de_DE/download.html) oder direkt über eine
[Weboberfläche](https://keepersecurity.eu/vault/#/lang/de_DE) auf das System zugreifen.

-   Beim ersten Anmelden ist die Oberfläche leer, da Sie noch in keinen Gruppen eingetragen sind.
    Dies ist erst möglich, nachdem Sie Ihr Konto angelegt haben. Schreiben Sie bitte eine kurze
    Email an die Email-Adresse am unteren Ende dieser Seite, damit wir Ihnen die korrekten Gruppen
    zuweisen können.

-   Innerhalb der Oberfläche finden Sie freigegebene Ordner mit Passwörtern für verschiedene Zwecke. Je
    nach Berechtigung können Sie diese nur anzeigen oder auch bearbeiten bzw. neue hinzufügen.

-   Einträge können durchaus in mehreren freigegebenen Ordern auftauchen. Dies wird verwendet, um
    den gleichen Eintrag mit verschiedenen Nutzern und unterschiedlichen Rechten zu teilen.
    
-   **Beim Anlegen eines neuen freigegebenen Ordner fügen Sie unbedingt die Gruppe "Keeper Admins" zu den
    Benutzern hinzu, wobei beide Rechte-Haken gesetzt werden sollten!**
    
-   Um einen existierenden Eintrag in einem weiteren Ordner freizugeben, klicken Sie oben rechts auf
    "Bearbeiten" und suchen dann den Eintrag im Feld "Datensätze hinzufügen".
    
-   Aus Sicherheitsgründen werden Sie aus den Clients nach einer halben Stunde automatisch abgemeldet.

## Passwortregeln

-   Verwenden Sie keine einfachen Passwörter, die aus Abwandlungen von Wörtern etc. bestehen. Diese
    Passwörter sind nicht ausreichend sicher!
    
-   Benutzen Sie zum Erstellen neuer Passwörter die im Keeper-Client eingebaute Funktion, die zufällige
    Passwörter liefert. Eine gute Passwort-Länge liegt bei ca. 16 Zeichen.
    
-   Verwenden Sie niemals das gleiche Passwort für mehrere Zugänge. Beispiel: Root- oder Admin-Passwörter
    sollten für jeden Rechner unterschiedlich sein.

## Fragen und Unterstützung

Bei Fragen zum System melden Sie sich bei der untenstehenden Email-Adresse.
